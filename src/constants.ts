import { environment } from './environments/environment';

export const APP: any = {
  AppBaseUrl: environment.api_url,
};
