import { Component, OnInit } from '@angular/core';
import { ArchivoService } from './archivos.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.css']
})
export class ArchivosComponent implements OnInit {
  public listColumnas: any[] = [];
  public listColumnasSelected = [];
  public delimitador = '';
  public campania = '';
  private archivo: File;
  constructor(private archivoService: ArchivoService) { }

  ngOnInit() {
  }

  async selectFiles(inputValue: any) {
    var file: File = inputValue.target.files[0];
    this.archivo = file;
    const temp = await this.readFileAsync(file);
    var lista = [];
    if (temp && temp.length > 0) {
      for (var i = 0; i < temp.length; i++) {
        if (temp[i] != '') {
          lista.push({ nombre: temp[i] });
        }
      }
    }
    this.listColumnas = lista;
  }
  async readFileAsync(file): Promise<any> {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      const delimiter = this.delimitador;
      reader.onload = function () {
        var result = reader.result;
        var texto = result.toString().substring(0, result.toString().indexOf("\n"))

        var temp = texto.toString().split(delimiter);
        if (temp.length > 0) {
          for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('\r') > 0) {
              temp[i] = temp[i].substring(0, temp[i].indexOf('\r'));
            }
          }
        }
        const resultado = temp //temp.split(/\n|\r/g);
        resolve(resultado);
      };

      reader.onerror = reject;

      reader.readAsText(file, "UTF-8");
    })
  }
  onChangeDelimiter() {
    this.campania = '';
    this.listColumnasSelected = [];
    this.listColumnas = [];
    this.archivo = undefined;
  }

  async cargarArchivo() {
    this.archivoService.upload(this.archivo, {
      delimitador: this.delimitador,
      codigo_campania: this.campania,
      columnas: JSON.stringify(this.listColumnasSelected)
    }, (result, err) => {
      if (err != null) {
        Swal.fire({
          icon: 'error',
          title: 'Error en el envío',
          text: `No se almacenó la información, descripción tecnica: ${err.error.error.message}`,
        });
      } else {
        Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: `Se ha almacenado la informacion`,
        });

      }
    });

  }
}

