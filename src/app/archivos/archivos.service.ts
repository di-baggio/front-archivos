import { Injectable } from '@angular/core';
import { APP } from '../../constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ArchivoService {
  apiUrl = APP.AppBaseUrl;

  constructor(
    private _http: HttpClient,
  ) { }
  public upload(file: File, data: any, callback) {
    const formData: FormData = new FormData();
    formData.append('archivo', file);
    formData.append('delimitador', data.delimitador);
    formData.append('columnas', data.columnas);
    formData.append('codigo_campania', data.codigo_campania);
    return this._http
      .post(`${this.apiUrl}/api/archivo/${file.name}`, formData)
      .subscribe(
        (result) => {
          callback(result, null);
        },
        (err) => {
          console.log(err);
          callback(null, err);
        }
      );
  }
}
