import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ArchivosComponent } from './archivos/archivos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {

  MatCardModule,
  MatToolbarModule,
  MatButtonModule,
} from "@angular/material";
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { FormsModule } from '@angular/forms';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ɵs } from '@ng-select/ng-select';
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    ArchivosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatToolbarModule,
    NgSelectModule,
    NgOptionHighlightModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule
  ],
  providers: [NgSelectConfig, ɵs],
  bootstrap: [AppComponent]
})
export class AppModule { }
